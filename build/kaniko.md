# kaniko

This template covers container image build using [kaniko](https://github.com/GoogleContainerTools/kaniko)

- `kaniko.yaml` contains template with some default env vars but it won't create any jobs on include
- `kaniko-common.yaml` contains jobs built upon `kaniko.yaml` template, you can reference it as an example or use it if it fits you needs

## Example

```yaml
include:
  - project: "pipeline-templates"
    ref: v1.0.0
    file:
      - "/build/kaniko.yaml"

.kaniko:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build:
  extends: .kaniko
  stage: build
```

This `include` will add the contents of [kaniko.yaml](./kaniko.yaml) to your pipeline definition. Note it contains only a template for the job but not job itself.

In example above we override `.kaniko` job template included from `kaniko.yaml`, we set parameters that will be the same for all the jobs inherited from this template. Next we define `build` job that uses `.build` template via `extends:` stanza and specify the particular parameters related for this very job.

## Description

Preparation command that defines credentials for Image Registry:

> Don't use these variables for the deploy as they are limited to the pipeline lifetime

```bash
echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
```

The main command is:

> `${KANIKO_ARGS}` forms a subset of switches defined by variables starting with `KANIKO_ARGS_`, see [How to add switches to the executable](#how-to-add-switches-to-the-executable)

```bash
/kaniko/executor ${KANIKO_ARGS}
```

Also there are [pre-defined variables](#pre-defined-variables)

### How to override jobs

You can override any item defined in the included template.

For example let's override `stage` and one of the pre-defined variables:

```yaml
build:
  stage: mybuild
  variables:
    KANIKO_ARGS_CONTEXT: "--context some_dir"
```

### How to add switches to the executable

If you need to add any additional cli parameters to kaniko use variables starting with prefix `KANIKO_ARGS_`, name could be anything you want. Example:

```yaml
build:
  variables:
    KANIKO_ARGS_NO_PUSH: "--no-push"
```

## Example Jobs

You can include jobs with:

```yaml
include:
  - project: "pipeline-templates"
    ref: v1.0.0
    file:
      - "/build/kaniko-common.yaml"
```

|Job|Description|
|--------|-----------|
|build|Image build only, without push (`KANIKO_PUSH_ARGS: --no-push`). Runs for the MR only|
|build_and_release|Build and push the image. Runs for the `$CI_DEFAULT_BRANCH` branch only|

## Pre-defined Variables

|Variable|Description|Default|
|--------|-----------|-------|
|`KANIKO_ARGS_CACHE`|Enables caching|`--cache=true`|
|`KANIKO_ARGS_CACHE_COPY_LAYERS`|Enables image layers caching|`--cache-copy-layers=true`|
|`KANIKO_ARGS_CACHE_TTL`|Sets cache time-to-live|`--cache-ttl=24h`|
|`KANIKO_ARGS_CACHE_REPO`|Defined where to store cache|`--cache-repo=${CI_REGISTRY_IMAGE}/cache`|
|`KANIKO_ARGS_DOCKERFILE`|relative path to Dockerfile|`--dockerfile ${CI_PROJECT_DIR}/Dockerfile`|
|`KANIKO_ARGS_DESTINATION`|image destination used on push|`--destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}`|
|`KANIKO_ARGS_CONTEXT`|context to build the image, default is project root|`--context ${CI_PROJECT_DIR}`|

## Reference

- https://github.com/GoogleContainerTools/kaniko