# buildkit

This template covers container image build using [buildkit](https://github.com/moby/buildkit)

- `buildkit.yaml` contains template with some default env vars but it won't create any jobs on include
- `buildkit-common.yaml` contains jobs built upon `buildkit.yaml` template, you can reference it as an example or use it if it fits you needs

## Example

```yaml
include:
  - project: "pipeline-templates"
    ref: v1.0.0
    file:
      - "/build/buildkit.yaml"

.buildkit:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build:
  extends: .buildkit
  stage: build
```

This `include` will add the contents of [buildkit.yaml](./buildkit.yaml) to your pipeline definition. Note it contains only a template for the job but not job itself.

In example above we override `.buildkit` job template included from `buildkit.yaml`, we set parameters that will be the same for all the jobs inherited from this template. Next we define `build` job that uses `.build` template via `extends:` stanza and specify the particular parameters related for this very job.

## Description

Preparation command that defines credentials for Image Registry:

> Don't use these variables for the deploy as they are limited to the pipeline lifetime

```bash
echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > ~/.docker/config.json
```

The main command is:

> `${BUILDKIT_ARGS}` forms a subset of switches defined by variables starting with `BUILDKIT_ARGS`, see [How to add switches to the executable](#how-to-add-switches-to-the-executable)

```bash
buildctl ${BUILDKIT_ADDR} build ${BUILDKIT_ARGS}
```

`${BUILDKIT_ADDR}` defines remote build executor using TCP. This is a global option and must be specified before `build`

Also there are [pre-defined variables](#pre-defined-variables)

### How to override jobs

You can override any item defined in the included template.

For example let's override `stage` and one of the pre-defined variables:

```yaml
build:
  stage: mybuild
  variables:
    BUILDKIT_ARGS_CONTEXT: "--local context=/some/dir"
```

### How to add switches to the executable

If you need to add any additional cli parameters to buildkit use variables starting with prefix `BUILDKIT_ARGS`, name could be anything you want. Example:

```yaml
build:
  variables:
    BUILDKIT_ARGS_BUILD_ARG: "--opt build-arg:APT_MIRROR=cdn-fastly.deb.debian.org"
```

## Example Jobs

You can include jobs with:

```yaml
include:
  - project: "pipeline-templates"
    ref: v1.0.0
    file:
      - "/build/buildkit-common.yaml"
```

|Job|Description|
|--------|-----------|
|build|Image build only, without push (`BUILDKIT_PUSH_OUTPUT: ""`). Runs for the MR only|
|build_and_release|Build and push the image. Runs for the `$CI_DEFAULT_BRANCH` branch only|

## Pre-defined Variables

|Variable|Description|Default|
|--------|-----------|-------|
|`BUILDKIT_ARGS_FRONTEND`|Defines [frontend](https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/syntax.md)|`--frontend=dockerfile.v0`|
|`BUILDKIT_ARGS_CACHE`|Enables caching|`--export-cache type=inline --import-cache type=registry,ref=${HARBOR_BASE_URL}/${HARBOR_PROJECT}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA}`|
|`BUILDKIT_ARGS_DOCKERFILE`|relative path to Dockerfile|`--local dockerfile=.`|
|`BUILDKIT_ARGS_OUTPUT`|image destination used on push|`--output type=image,name=${HARBOR_BASE_URL}/${HARBOR_PROJECT}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA},push=true`|
|`BUILDKIT_ARGS_CONTEXT`|context to build the image, default is project root|`--local context=.`|

## Reference

- https://github.com/moby/buildkit