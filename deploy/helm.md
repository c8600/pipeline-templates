# helm

This template covers deploy using [helm](https://helm.sh/)

It's impossible to cover all the possible scenarios. Template covers basic usage but it can be extended to cover more advanced ones.

- `helm.yaml` contains template with some default env vars but it won't create any jobs on include
- `helm-common.yaml` contains jobs built upon `helm.yaml` template, you can reference it as an example or use it if it fits you needs

Contact repo [maintainers](./../CODEOWNERS) if you'd like to add new templates

## Example

```yaml
include:
  - project: "pipeline-templates"
    ref: main
    file:
      - "/deploy/helm.yaml"

.deploy:
  variables:
    HELM_CHART_PATH: helm
    HELM_ARGS_TIMEOUT: "--timeout 15m0s"
    HELM_ARGS_IMAGE: >
      --set global.image.pullSecret=${CI_PROJECT_NAME}-registry
      --set global.image.Repository=${CI_REGISTRY_IMAGE}/
      --set global.image.Tag=$TAG
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:stage:
  stage: deploy
  extends:
    - .deploy
  environment:
    name: staging
    url: https://app.example.com
  variables:
    HELM_RELEASE_NAME: ${CI_PROJECT_NAME}
    HELM_NAMESPACE: ${CI_PROJECT_NAME}-dev
    HELM_ARGS_VALUES: "-f helm/values/staging.yaml"

```

This `include` will add the contents of [helm.yaml](./helm.yaml) to your pipeline definition. Note it contains only a template for the job but not job itself.

In example above we override `.deploy` job template included from `helm.yaml`, we set parameters that will be the same for all the jobs inherited from this template. Next we define `deploy:stage` job that uses `.deploy` template via `extends:` stanza and specify the particular parameters related for this very job.

> - `${HELM_ARGS}` forms a subset of switches defined by variables starting with `HELM_ARGS_`, see [How to add switches to the executable](#how-to-add-switches-to-the-executable)
> - `${HELM_EXTRA_ARGS}` is a variable to add additional parameters to CLI if `HELM_ARGS_` is not enough

The main command is:

```bash
helm upgrade \
  "${HELM_RELEASE_NAME:?variable is not set}" \
  ${HELM_CHART_PATH:?variable is not set} \
  --namespace ${HELM_NAMESPACE:?variable is not set} \
  ${HELM_ARGS}
  ${HELM_EXTRA_ARGS}
```

There are 3 mandatory variables:

- `HELM_RELEASE_NAME` - defines the helm release name
- `HELM_CHART_PATH` - defines the path to Helm chart
- `HELM_NAMESPACE` - defines the namespace where helm should deploy the release

Also there are [pre-defined variables](#pre-defined-variables)

### How to override jobs

You can override any item defined in the included template.

For example let's override `stage` for `deploy:stage` job and disable one of the pre-defined variables

```yaml
deploy:stage:
  stage: mydeploy
  variables:
    HELM_ARGS_ATOMIC: ""
```

### How to add switches to the executable

If you need to add any additional CLI parameters to Helm use variables starting with prefix `HELM_ARGS_`, name could be anything you want.

Let's add a `--debug` cli parameter for Helm:

```yaml
deploy:stage:
  variables:
    HELM_ARGS_DEBUG: "--debug"
```

### How to disable preview deploy

You need to override `rules` section in `preview` jobs:

```yaml
deploy:review:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never

stop:review:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
```

## Example Jobs

You can include jobs with:

```yaml
include:
  - project: "pipeline-templates"
    ref: main
    file:
      - "/deploy/helm-common.yaml"
```

|Job|Description|
|--------|-----------|
|deploy:review|Enabled by default, triggered in `merge_requrest` pipelines only, intended to be used to deploy automatically to preview dynamic environments|
|stop:review|Enabled by default, manual, triggered in `merge_requrest` pipelines only, will run automatically on branch delete (mentioned in MR)|
|deploy:stage|Enabled by default, triggered in default branch only (e.g. `main`, `main`), intended to be used to deploy automatically to dev/stage environments|
|deploy:prod|Enabled by default, triggered in default branch only (e.g. `main`, `main`), intended to be used to deploy manually to prod environments|

## Pre-defined Variables

|Variable|Description|Default|
|--------|-----------|-------|
|`HELM_ARGS_ATOMIC`|Enables automatic rollback on failure|`--atomic`|
|`HELM_ARGS_CLEANUP_ON_FAIL`|Enables resource cleanup on failure|`--cleanup-on-fail`|
|`HELM_ARGS_INSTALL`|Enables install if release doesn't exist|`--install`|

## Reference

- https://github.com/GoogleContainerTools/kaniko