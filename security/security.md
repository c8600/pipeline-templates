# security

- `security.yaml` - security jobs definition. Can be used as direct include to the pipeline (note: `test` stage is required until it has been overriden)
- `security-pipeline.yaml` - definition to be used if child pipeline approach is used
- `security-common.yaml` - defeinition to include if one wnat to add security child pipeline