# pipeline-templates

This repo contain templates that could be imported in pipelines using GitLab [include:file](https://docs.gitlab.com/ee/ci/yaml/#includefile)

`main` is a development branch, releases will be done via `tags`

## Example

```yaml
include:
  - project: "pipeline-templates"
    ref: v1.0.0
    file:
      - "/build/kaniko.yaml"
  - project: "pipeline-templates"
    ref: main
    file:
      - "/deploy/helm.yaml"
```

It will include contents of [/build/kaniko.yaml](https://gitlab.com/c8600/pipeline-templates/-/blob/v1.0.0/build/kaniko.yaml) from [v1.0.0](https://gitlab.com/c8600/pipeline-templates/-/tree/v1.0.0) branch of [pipeline-templates](https://gitlab.com/c8600/pipeline-templates) project and contents of the `/deloy/helm.yaml` from `main` branch

Any job defined in template could be modified to your needs, you can override or add any item:

```yaml
build:
  stage: somestage
```

In this case `build` job will inherit all the lines from template but `stage` will be overriden to `somestage`

## Templates

- Build
  - [kaniko](./build/kaniko.md)
  - [buildkit](./build/buildkit.md)
- Deploy
  - [helm](./deploy/helm.md)
- Generic
  - [colors](./generic/colors.md)
- Security
  - [security](./security/security.md)

## Reference

- https://docs.gitlab.com/ee/ci/yaml/#includefile
