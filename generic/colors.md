# colors

This template defines variables that could be used to colorify GitLab job output

## Example

```yaml
include:
  - project: "templates/pipeline"
    ref: main
    file:
      - "/generic/colors.yaml"

hello:color:
  stage: build
  image: alpine
  script:
    - |
      cat > msg <<EOF
       
      ${TXT_LYELLOW}Hello${TXT_NORMAL}, ${TXT_LCYAN} Color${TXT_NORMAL}!
       
      EOF
    - cat msg
```

## Reference

- https://misc.flogisoft.com/bash/tip_colors_and_formatting
- https://docs.gitlab.com/ee/ci/yaml/script.html#add-color-codes-to-script-output
